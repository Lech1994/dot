﻿using HogwartLibrary.Data.Repositories;
using HogwartLibrary.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HogwartLibrary.Web.Controllers
{
    public class BooksController : Controller
    {
        UnitOfWork _uow;
        static int _pageSize = 4;

        public BooksController()
        {
            _uow = new UnitOfWork();
        }

        public ActionResult Index(int page = 0)
        {
            var books = _uow.BookRepository
                .Get()
                .Skip(page*_pageSize)
                .Take(_pageSize);
            var viewModel = new BooksListViewModel
            {
                Books = books,
                PagingInfo = new PagingInfo 
                {
                    CurrentPage = page,
                    ItemsPerPage = _pageSize,
                    TotalItems = _uow.BookRepository.Get().Count()
                }
            };
            return View(viewModel);
        }
    }
}