﻿using HogwartLibrary.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HogwartLibrary.Data.Repositories
{
    public class HogwartContext:DbContext
    {
        public HogwartContext() : base("name=Hogwart") { }

        public DbSet<Book> Books { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
