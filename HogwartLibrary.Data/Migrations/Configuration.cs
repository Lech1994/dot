namespace HogwartLibrary.Data.Migrations
{
    using HogwartLibrary.Data.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HogwartLibrary.Data.Repositories.HogwartContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HogwartLibrary.Data.Repositories.HogwartContext context)
        {
            var book1 = new Book
            {
                Price = (decimal)12.34,
                Title = "Your First FireBall !"
            };
            var book2 = new Book
            {
                Price = (decimal)12.34,
                Title = "Creating Love potion from scratch"
            };
            var book3 = new Book
            {
                Price = (decimal)12.34,
                Title = "Advanced Necromancy"
            };
            var book4 = new Book
            {
                Price = (decimal)12.34,
                Title = "Mind control for beginners"
            };
            var book5 = new Book
            {
                Price = (decimal)12.34,
                Title = "How to deafeat an Angry Dementor"
            };
            var book6 = new Book
            {
                Price = (decimal)12.34,
                Title = "Crafting your own Wand."
            };
            var book7 = new Book
            {
                Price = (decimal)12.34,
                Title = "Protection Spells. Excercises with ready solutions"
            };
            var book8 = new Book
            {
                Price = (decimal)12.34,
                Title = "Black magic for Dummies"
            };
            var book9 = new Book
            {
                Price = (decimal)12.34,
                Title = "Wand First! Nature magic"
            };
            var book10 = new Book
            {
                Price = (decimal)12.34,
                Title = "Great artifacts of the century"
            };
            var book11 = new Book
            {
                Price = (decimal)12.34,
                Title = "How to tame a troll"
            };
            var category = new Category
            {
                Name = "Magic",
                Books = new[] 
                { 
                    book1, 
                    book2,
                    book3,
                    book4,
                    book5,
                    book6,
                    book7,
                    book8,
                    book9,
                    book10,
                    book11
                }
            };
            var category1 = new Category
            {
                Name = "White Magic",
                Books = new[] 
                { 
                    book1, 
                    book6,
                    book7,
                    book9,
                }
            };
            var category2 = new Category
            {
                Name = "Black Magic",
                Books = new[] 
                { 
                    book3,
                    book8,
                }
            };
            var category3 = new Category
            {
                Name = "Potions",
                Books = new[] 
                {  
                    book2
                }
            };
            var category4 = new Category
            {
                Name = "Magic Items",
                Books = new[] 
                { 
                    book6,
                    book9,
                    book10
                }
            };

            context.Categories.Add(category);
            context.Categories.Add(category1);
            context.Categories.Add(category2);
            context.Categories.Add(category3);
            context.Categories.Add(category4);
            context.SaveChanges();
          

            
        }
    }
}
